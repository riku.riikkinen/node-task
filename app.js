const express = require('express')
const app = express()

/**
 * add two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number} sum of a & b
 */

const add = (a,b) => {
    return a + b
}

app.get('', (req,res) => {
    const sum = add(1,2)
    console.log(sum)

    res.send('ok')
})

app.listen(3000, () => {
    console.log('server listening on localhost:3000')
})